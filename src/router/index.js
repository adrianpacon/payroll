import Vue from 'vue'
import Router from 'vue-router'
import responsive from 'vue-responsive'
// import HelloWorld from '@/components/HelloWorld'
import Printable from '@/components/Printable'
import Sample from '@/components/Sample'
// import Crud from '@/components/Crud'

Vue.use(Router)
Vue.use(responsive)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Sample',
      component: Sample
    },
    {
      path: '/print',
      name: 'Printable',
      component: Printable
    }
  ]
})
